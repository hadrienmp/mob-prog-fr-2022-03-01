module Tests exposing (..)

import Expect
import Test exposing (..)


type Indice
    = Inconnu Char
    | Correct Char


type État
    = Gagné Détail
    | EssaieEncore Détail
    | Perdu DétailPerdu


type alias Détail =
    { essaisRestant : Int
    , indices : List Indice
    }


type alias DétailPerdu =
    { indices : List Indice
    }


pendu : String -> État
pendu mot =
    EssaieEncore
        { essaisRestant = 11
        , indices = String.split "" mot |> List.map toChar |> List.map Inconnu
        }


toChar : String -> Char
toChar mot =
    String.uncons mot
        |> Maybe.map Tuple.first
        |> Maybe.withDefault '?'


essai : Char -> État -> État
essai lettre état =
    case état of
        Gagné _ ->
            état

        Perdu _ ->
            état

        EssaieEncore détail ->
            let
                premiereLettre =
                    List.head détail.indices
                        |> Maybe.withDefault (Inconnu '?')
            in
            if Inconnu lettre == premiereLettre && List.length détail.indices > 1 then
                EssaieEncore
                    { détail
                        | indices =
                            List.map
                                (\indice ->
                                    if indice == Inconnu lettre then
                                        Correct lettre

                                    else
                                        indice
                                )
                                détail.indices
                    }

            else if Inconnu lettre == premiereLettre then
                Gagné
                    { détail
                        | indices = [ Correct lettre ]
                    }

            else if détail.essaisRestant == 1 then
                Perdu { indices = détail.indices }

            else
                EssaieEncore { détail | essaisRestant = détail.essaisRestant - 1 }


suite : Test
suite =
    describe "pendu"
        [ describe "d'une lettre"
            [ test "un échec" <|
                \_ ->
                    pendu "a"
                        |> essai 'b'
                        |> Expect.equal
                            (EssaieEncore
                                { essaisRestant = 10
                                , indices = [ Inconnu 'a' ]
                                }
                            )
            , test "deux échecs" <|
                \_ ->
                    pendu "a"
                        |> essai 'b'
                        |> essai 'c'
                        |> Expect.equal
                            (EssaieEncore
                                { essaisRestant = 9
                                , indices = [ Inconnu 'a' ]
                                }
                            )
            , test "réussite du premier coup" <|
                \_ ->
                    pendu "a"
                        |> essai 'a'
                        |> Expect.equal
                            (Gagné
                                { essaisRestant = 11
                                , indices = [ Correct 'a' ]
                                }
                            )
            ]
        , describe "de deux lettres différentes"
            [ test "un échec" <|
                \_ ->
                    pendu "ab"
                        |> essai 'c'
                        |> Expect.equal
                            (EssaieEncore
                                { essaisRestant = 10
                                , indices = [ Inconnu 'a', Inconnu 'b' ]
                                }
                            )
            , test "un échec 2" <|
                \_ ->
                    pendu "ab"
                        |> essai 'a'
                        |> Expect.equal
                            (EssaieEncore
                                { essaisRestant = 11
                                , indices = [ Correct 'a', Inconnu 'b' ]
                                }
                            )
            ]
        , test "3 lettres, première lettre trouvée" <|
            \_ ->
                pendu "abc"
                    |> essai 'a'
                    |> Expect.equal
                        (EssaieEncore
                            { essaisRestant = 11
                            , indices = [ Correct 'a', Inconnu 'b', Inconnu 'c' ]
                            }
                        )
        , test "asfasdg" <|
            \_ ->
                pendu "acab"
                    |> essai 'a'
                    |> Expect.equal
                        (EssaieEncore
                            { essaisRestant = 11
                            , indices = [ Correct 'a', Inconnu 'c', Correct 'a', Inconnu 'b' ]
                            }
                        )
        , describe "règles de gestion"
            [ test "après 11 essais ratés on est mort" <|
                \_ ->
                    pendu "a"
                        |> essais 11 'b'
                        |> Expect.equal
                            (Perdu
                                { indices = [ Inconnu 'a' ]
                                }
                            )
            , test "gagné est un état final" <|
                \_ ->
                    pendu "a"
                        |> essai 'a'
                        |> essai 'b'
                        |> Expect.equal
                            (Gagné
                                { essaisRestant = 11
                                , indices = [ Correct 'a' ]
                                }
                            )
            , test "perdu est un état final" <|
                \_ ->
                    pendu "a"
                        |> essais 11 'b'
                        |> essai 'b'
                        |> Expect.equal
                            (Perdu
                                { indices = [ Inconnu 'a' ]
                                }
                            )
            ]
        ]


essais : Int -> Char -> État -> État
essais xFois lettre initial =
    essai lettre
        |> List.repeat xFois
        |> List.foldl (\courant acc -> courant acc) initial
